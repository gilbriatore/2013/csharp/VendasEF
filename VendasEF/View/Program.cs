﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasEF.DAO;
using VendasEF.Model;
using VendasEF.Negocio;

namespace VendasEF.View
{
    class Program
    {
        static void Main(string[] args)
        {
            int opc;
            do
            {
                Console.WriteLine("\n\n1 - Cadastrar clientes");
                Console.WriteLine("2 - Cadastrar vendedores");
                Console.WriteLine("3 - Cadastrar produtos");
                Console.WriteLine("4 - Realizar venda");
                Console.WriteLine("5 - Consultar venda");
                Console.WriteLine("6 - Sair");
                Console.Write("Opção: ");
                opc = int.Parse(Console.ReadLine());
                switch (opc)
                {
                    case 1:
                        CadastrarClientes();
                        break;
                    case 2:
                        CadastrarVendedores();
                        break;
                    case 3:
                        CadastrarProdutos();
                        break;
                    case 4:
                        RealizarVendas();
                        break;
                    case 5:
                        ConsultarVendas();
                        break;
                }
            } while (opc != 6);
        }

        private static void CadastrarClientes()
        {
            Console.WriteLine("\n\n");
            Cliente cliente = new Cliente();
            Console.Write("CPF: ");
            cliente.Cpf = Console.ReadLine();
            if (ClienteNegocio.ValidarCpf(cliente))
            {
                if (ClienteDAO.Search(cliente) == null)
                {
                    Console.Write("Nome: ");
                    cliente.Nome = Console.ReadLine();
                    ClienteDAO.Insert(cliente);
                }
                else
                {
                    Console.WriteLine("\n\nCliente já cadastrado.");
                }
            }
            else
            {
                Console.WriteLine("\n\nCPF inválido.");
            }
        }

        private static void CadastrarVendedores()
        {
            Vendedor vendedor = new Vendedor();
            Console.WriteLine("\n\n");
            Console.Write("CPF: ");
            vendedor.Cpf = Console.ReadLine();
            if (VendedorNegocio.ValidarCpf(vendedor))
            {
                if (VendedorDAO.Search(vendedor) == null)
                {
                    Console.Write("Nome: ");
                    vendedor.Nome = Console.ReadLine();
                    Console.Write("Taxa de comissão: ");
                    vendedor.Taxa = int.Parse(Console.ReadLine());
                    VendedorDAO.Insert(vendedor);
                }
                else
                {
                    Console.WriteLine("\n\nVendedor já cadastrado.");
                }
            }
            else
            {
                Console.WriteLine("\n\nCPF inválido.");
            }
        }

        private static void CadastrarProdutos()
        {
            Console.WriteLine("\n\n");
            Produto produto = new Produto();
            Console.Write("Produto: ");
            produto.Nome = Console.ReadLine();
            if (ProdutoDAO.Search(produto) == null)
            {
                Console.Write("Preço de compra: ");
                produto.PrecoCompra = float.Parse(Console.ReadLine());
                Console.Write("Markup: ");
                produto.Markup = float.Parse(Console.ReadLine());
                ProdutoDAO.Insert(produto);
            }
            else
            {
                Console.WriteLine("\n\nProduto já cadastrado.");
            }
        }

        private static void RealizarVendas()
        {
            Console.WriteLine("\n\n");
            string op = "";
            Venda venda = new Venda();
            Cliente cliente = new Cliente();
            Console.Write("Cliente: ");
            cliente.Cpf = Console.ReadLine();
            cliente = ClienteDAO.Search(cliente);
            if (cliente != null)
            {
                venda.cliente = cliente;
                Vendedor vendedor = new Vendedor();
                Console.Write("Vendedor: ");
                vendedor.Cpf = Console.ReadLine();
                vendedor = VendedorDAO.Search(vendedor);
                if (vendedor != null)
                {
                    venda.Vendedor = vendedor;
                    venda.DataVenda = DateTime.Now;
                    List<ItemVenda> itens = new List<ItemVenda>();
                    do
                    {
                        ItemVenda item = new ItemVenda();
                        Produto produto = new Produto();
                        Console.Write("Produto: ");
                        produto.Nome = Console.ReadLine();
                        produto = ProdutoDAO.Search(produto);
                        if (produto != null)
                        {
                            item.Produto = produto;
                            Console.Write("Quantidade: ");
                            item.Quantidade = int.Parse(Console.ReadLine());
                            item.Unitario = ItemVendaNegocio.CalcularPrecoVenda(item);
                            Console.WriteLine("Unitário: " + item.Unitario.ToString("C2"));
                            itens.Add(item);                            
                        }
                        else
                        {
                            Console.WriteLine("\n\nProduto não cadastrado.");
                        }
                        Console.Write("Outro produto? ");
                        op = Console.ReadLine();
                    } while (op.Equals("s"));
                    venda.Itens = itens;
                    VendaDAO.Insert(venda);
                }
                else
                {
                    Console.WriteLine("\n\nVendeedor não cadastrado.");
                }
            }
            else
            {
                Console.WriteLine("\n\nCliente não cadastrado.");
            }
        }

        private static void ConsultarVendas()
        {
            Console.WriteLine("\n\n");
            Venda venda = new Venda();
            Console.Write("ID: ");
            venda.Id = int.Parse(Console.ReadLine());
            venda = VendaDAO.Search(venda);
            if (venda != null)
            {
                Console.WriteLine("Cliente: " + venda.cliente.Nome);
                Console.WriteLine("Vendedor: " + venda.Vendedor.Nome);
                Console.WriteLine("Data: " + venda.DataVenda);
                Console.WriteLine("--------------------------------");
                foreach(ItemVenda x in venda.Itens)
                {
                    Console.WriteLine("Produto: " + x.Produto.Nome);
                    Console.WriteLine("Quantidade: " + x.Quantidade);
                    Console.WriteLine("Unitário: " + x.Unitario.ToString("C2"));
                    Console.WriteLine("Subtotal: " + ItemVendaNegocio.CalcularSubTotal(x).ToString("N2"));
                    Console.WriteLine("--------------------------------");
                }
                Console.WriteLine("Total: " + VendaNegocio.CalcularTotal(venda.Itens).ToString("N2"));
            }
        }
    }
}
