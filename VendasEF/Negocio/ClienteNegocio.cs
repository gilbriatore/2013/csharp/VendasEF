﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasEF.Model;
namespace VendasEF.Negocio
{
    class ClienteNegocio
    {
        public static bool ValidarCpf(Cliente cliente)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;

            cliente.Cpf = cliente.Cpf.Trim();
            cliente.Cpf = cliente.Cpf.Replace(".", "").Replace("-", "");

            if (cliente.Cpf.Length != 11)
                return false;

            tempCpf = cliente.Cpf.Substring(0, 9);
            soma = 0;
            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();

            tempCpf = tempCpf + digito;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cliente.Cpf.EndsWith(digito);
        }
    }
}
