﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendasEF.Model
{
    class Produto
    {
        public int Id { set; get; }
        public string Nome { set; get; }
        public float PrecoCompra { set; get; }
        public float Markup { set; get; }
    }
}
