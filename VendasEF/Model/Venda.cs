﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendasEF.Model
{
    class Venda
    {
        public int Id { set; get; }
        public virtual Cliente cliente { set; get; }
        public virtual Vendedor Vendedor { set; get; }
        public DateTime DataVenda { set; get; }
        public virtual ICollection<ItemVenda> Itens { set; get; }        
    }
}
