﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendasEF.Model
{
    class ItemVenda
    {
        public int Id { set; get; }
        public virtual Produto Produto { set; get; }
        public int Quantidade { set; get; }
        public float Unitario { set; get; }
    }
}
