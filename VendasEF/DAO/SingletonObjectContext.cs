﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendasEF.DAO
{
    class SingletonObjectContext
    {
        private static readonly SingletonObjectContext instance = new SingletonObjectContext();
        private readonly VendaEntities context;

        private SingletonObjectContext()
        {
            context = new VendaEntities();
        }

        public static SingletonObjectContext Instance
        {
            get
            {
                return instance;
            }
        }

        public VendaEntities Context
        {
            get
            {
                return context;
            }
        }        
    }
}
