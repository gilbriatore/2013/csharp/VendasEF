﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasEF.Model;
namespace VendasEF.DAO
{
    class VendedorDAO
    {
        public static bool Insert(Vendedor vendedor)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Vendedores.Add(vendedor);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Vendedor Search(Vendedor vendedor)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                return db.Vendedores.FirstOrDefault(x => x.Cpf.Equals(vendedor.Cpf));
            }
            catch
            {
                return null;
            }
        }
    }
}
