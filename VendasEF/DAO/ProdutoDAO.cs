﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasEF.Model;
namespace VendasEF.DAO
{
    class ProdutoDAO
    {
        public static bool Insert(Produto produto)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Produtos.Add(produto);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Produto Search(Produto produto)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                return db.Produtos.FirstOrDefault(x => x.Nome.Equals(produto.Nome));
            }
            catch
            {
                return null;
            }
        }
    }
}
