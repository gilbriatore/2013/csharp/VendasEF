﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasEF.Model;

namespace VendasEF.DAO
{
    class ClienteDAO
    {
        public static bool Insert(Cliente cliente)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Cliente Search(Cliente cliente)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                return db.Clientes.FirstOrDefault(x => x.Cpf.Equals(cliente.Cpf));
            }
            catch
            {
                return null;
            }
        }
    }
}
