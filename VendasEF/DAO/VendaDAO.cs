﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendasEF.Model;
namespace VendasEF.DAO
{
    class VendaDAO
    {
        public static bool Insert(Venda venda)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Vendas.Add(venda);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Venda Search(Venda venda)
        {
            VendaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                venda =  db.Vendas.Include("Cliente").Include("Vendedor").Include("Itens").Include("Itens.Produto").FirstOrDefault(x => x.Id == venda.Id);
                return venda;
            }
            catch
            {
                return null;
            }
        }
    }
}
