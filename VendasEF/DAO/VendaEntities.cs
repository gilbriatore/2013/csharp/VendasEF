﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using VendasEF.Model;

namespace VendasEF.DAO
{
    class VendaEntities : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ItemVenda>().ToTable("ItensVenda");
            modelBuilder.Entity<Produto>().ToTable("Produtos");
            modelBuilder.Entity<Vendedor>().ToTable("Vendedores");

            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Cliente> Clientes { set; get; }
        public DbSet<Produto> Produtos { set; get; }
        public DbSet<Vendedor> Vendedores { set; get; }
        public DbSet<Venda> Vendas { set; get; }
        public DbSet<ItemVenda> ItensVenda { set; get; }
    }
}
